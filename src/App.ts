import * as express from "express";
import { Controller } from "./Controller";
import { ControllerClient } from "./client/ControllerClient";
export class App{
    private app: any;
    private controller: Controller;
    //private controllerClient: ControllerClient;
    //8th line
    private static port: number = 3000;

    private constructor() {
        this.app = express();
        this.controller = new Controller();
        //this.controllerClient = new ControllerClient
        
    }

    public static newInstance(){
        return new App();
    }

    public startRouter():void{
        this.app.use(express.json());
        this.app.get("/", this.controller.showIndexPage);
        this.app.get(/(images|styles|scripts)/, this.controller.showImagesAndCSS);

        this.app.get("/products", this.controller.getAllProducts);
        this.app.post("/products", this.controller.addProduct);

        this.app.post("/products/init", this.controller.initProducts);

        this.app.get("/products/:id", this.controller.getProductById);
        this.app.put("/products/:id", this.controller.updateProductById);
        this.app.delete("/products/:id", this.controller.deleteProductById);

        this.app.all("*", this.controller.showErrorPage);
        this.app.use(this.controller.doErrorHandling); 

        this.app.listen(App.port, function(){
            console.log("webserver app listening inb port: "+ App.port)
        });
    }
}
