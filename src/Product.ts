export class Product{
    private static lastId: number = 1;
    private static filterId: number =-1;
    private static filterCategory: string;
    private static filterColor: string;
    private id: number;
    private name: string;
    private category: string;
    private color: string;

    public constructor(name: string, category: string, color: string){
        this.id = Product.lastId;
        this.name = name;
        this.category = category;
        this.color = color;
        Product.lastId++;
    }

    public static fromJson(data: any){
        let retProd: Product = new Product(data.name, data.category, data.color);
        retProd.id = data.id;
        return retProd;
    }

    public getId(): number{
        return this.id;
    }
    public getName(): string{
        return this.name;
    }
    public getColor(): string{
        return this.color;
    }
    public getCategory():string{
        return this.category;
    }
    public toString():string{
        return "Product: [" + this.id +", " + this.name + ", " + this.category + ", " +this.color + "]";  
    }
    setColor(color: string) {
        this.color = color;
    }
    setCate(category: string) {
        this.category = category;
    }
    setName(name: string) {
        this.name = name;
    }
}
