import { Product } from "./Product";
export class Database{
    private static collProducts: Array<Product>;
    public constructor(){

    }

    public initProducts(): void{
        Database.collProducts = new Array<Product>();
        try {
            Database.collProducts.push(new Product("Notebook", "tool", "black"));
            Database.collProducts.push(new Product("Monitor", "tool", "grey"));
            Database.collProducts.push(new Product("Borgward", "vehicle", "red"));
            Database.collProducts.push(new Product("Bike", "vehicle", "black"));
            Database.collProducts.push(new Product("Ski", "tool", "red"));
            Database.collProducts.push(new Product("Socks", "other", "red"));
            console.log("==== init done")
        } catch (error) {
            console.log("===== unexpexted error happened: " + (error as Error).message + "( " + (error as Error).name + ")");
        }
    }
    public getAllProducts(category: string, color: string):Array<Product> {
        let products:Product[];
        if(category != null && color == null || category.length >0 && color.length <=0){
            products = Database.collProducts.filter(x => x.getCategory() == category);
        } 
        else if (category == null && color != null || category.length <=0 && color.length >0){
            products = Database.collProducts.filter(x => x.getColor() == color);
        }
        else if(category != null && color != null){
            products = Database.collProducts.filter(x => x.getCategory() == category && x.getColor() == color);
        }
        else{
            products = Database.collProducts;
        }
        return products;
    }
    
    public getProductById(nr: number):Product {
    let product: Product = Database.collProducts.find(x => x.getId() == nr);
        if(product == null){
            throw new Error("no product found with id: " + nr);
        }
    return product;
    }
    deleteProductByIdDB(id: number):Product {
        
        let p: Product = Database.collProducts.find(x => x.getId() == id);
        if(p == null){
            throw new Error("no product found with id: " + id);
        }
        Database.collProducts.splice(id-1,1);
        return p;
    }
    public addProduct(p: Product){
        let retProd: Product = new Product(p.getName(), p.getCategory(), p.getColor());
        console.log(retProd.toString());
        Database.collProducts.push(retProd);
        return retProd;
    }
    updateProd(p: Product, id: number) {

        if(Database.collProducts[id-1] == null){
            throw new Error("no product found with id: " + id);
        }
        Database.collProducts[id-1].setName(p.getName());
        Database.collProducts[id-1].setCate(p.getCategory());
        Database.collProducts[id-1].setColor(p.getColor());
    }
}