import { Database } from "./Database";
import { AppError } from "./AppError";
import { Product } from "./Product";
import { FileOption } from "./FileOption";
export class Controller{
    private static db: Database;
    private static option: FileOption;

    public constructor(){
        Controller.option = new FileOption();
        Controller.option.root = "/AAIF/NFS/Semester5_6_NVS/05Nodes/Overview/02SimpleServerB/wwwPublic";
        Controller.db = new Database();
    }

    public initProducts(req, res):void{
        Controller.db.initProducts();
        console.log("==== init done");
        res.json("==init done==");
    }

    public showErrorPage(req, res): void{
        console.log("=====error page ..." + req.originalUrl);
        res.status(404).json({
            message: "Can't find " + req.originalUrl + " on this server",
            status: "fail",
        });
    }
    public doErrorHandling(err: AppError, req, res, next):void {
        console.log("=====error handling ..." + req.originalUrl + "..." + JSON.stringify(err));
        err.statusCode = err.statusCode || 500;
        err.status = err.status || "unknown Error";
        res.status(err.statusCode).json(err);
    }

    public getAllProducts(req, res) {
        let collFilteredProducts = Controller.db.getAllProducts(req.query.category, req.query.color);
        console.log("===== get all or filtered product");
        res.json(collFilteredProducts);
    }

    public getProductById(req, res, next){
        let filteredProd: Product;
        try{
            filteredProd = Controller.db.getProductById(req.params.id);
        }catch(error){
            let err: AppError = new AppError((error as Error).message);
            err.status ="fail";
            err.statusCode = 404;
            next(err);
        }
        console.log("===== get product by id");
        res.json(filteredProd);
    }

    public deleteProductById(req, res, next) {
        let filteredProd: Product;
        try{
            let deleledProd:Product =  Controller.db.deleteProductByIdDB(req.params.id);
            console.log("Prodact: " + deleledProd.toString() + " was deleted");
            res.json("Prodact: " + deleledProd.toString() + " was deleted");
        }catch(error){
            let err: AppError = new AppError((error as Error).message);
            err.status ="fail";
            err.statusCode = 404;
            next(err);
        }
        console.log("===== get product by id");
        res.json(filteredProd);
    }
    public updateProductById(req, res, next) {
        try{
            let newProduct: Product = Product.fromJson(req.body);
            let id:number = req.params.id;
            Controller.db.updateProd(newProduct, id);
            console.log("Update done");
            res.json("update done to: " + newProduct.toString());
        }catch(error){
        let err: AppError = new AppError("error: "+ (error as Error).message);
        err.status ="fail";
        err.statusCode = 404;
        next(err);
    }

    }
    public addProduct(req, res, next) {
        try{
            let newProduct: Product = Product.fromJson(req.body);
            let retProduct = Controller.db.addProduct(newProduct);
            console.log("===== add Product: " + newProduct.toString());
            res.json(retProduct);
        }catch(error){
            let err: AppError = new AppError("error: "+ (error as Error).message);
            err.status ="fail";
            err.statusCode = 400;
            next(err);
        }

    }
    public showImagesAndCSS(req, res, next) {
        let url:string = req.originalUrl;
        console.log("=====show images/CSS..." + url);
        console.log();
        res.sendFile(url, Controller.option);
    }
    public showIndexPage(req, res, next) {
        res.sendFile("html/index.html", Controller.option);
        console.log("===== show index.html");
    }
}