"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Database = void 0;
const Product_1 = require("./Product");
class Database {
    constructor() {
    }
    initProducts() {
        Database.collProducts = new Array();
        try {
            Database.collProducts.push(new Product_1.Product("Notebook", "tool", "black"));
            Database.collProducts.push(new Product_1.Product("Monitor", "tool", "grey"));
            Database.collProducts.push(new Product_1.Product("Borgward", "vehicle", "red"));
            Database.collProducts.push(new Product_1.Product("Bike", "vehicle", "black"));
            Database.collProducts.push(new Product_1.Product("Ski", "tool", "red"));
            Database.collProducts.push(new Product_1.Product("Socks", "other", "red"));
            console.log("==== init done");
        }
        catch (error) {
            console.log("===== unexpexted error happened: " + error.message + "( " + error.name + ")");
        }
    }
    getAllProducts(category, color) {
        let products;
        if (category != null && color == null || category.length > 0 && color.length <= 0) {
            products = Database.collProducts.filter(x => x.getCategory() == category);
        }
        else if (category == null && color != null || category.length <= 0 && color.length > 0) {
            products = Database.collProducts.filter(x => x.getColor() == color);
        }
        else if (category != null && color != null) {
            products = Database.collProducts.filter(x => x.getCategory() == category && x.getColor() == color);
        }
        else {
            products = Database.collProducts;
        }
        return products;
    }
    getProductById(nr) {
        let product = Database.collProducts.find(x => x.getId() == nr);
        if (product == null) {
            throw new Error("no product found with id: " + nr);
        }
        return product;
    }
    deleteProductByIdDB(id) {
        let p = Database.collProducts.find(x => x.getId() == id);
        if (p == null) {
            throw new Error("no product found with id: " + id);
        }
        Database.collProducts.splice(id - 1, 1);
        return p;
    }
    addProduct(p) {
        let retProd = new Product_1.Product(p.getName(), p.getCategory(), p.getColor());
        console.log(retProd.toString());
        Database.collProducts.push(retProd);
        return retProd;
    }
    updateProd(p, id) {
        if (Database.collProducts[id - 1] == null) {
            throw new Error("no product found with id: " + id);
        }
        Database.collProducts[id - 1].setName(p.getName());
        Database.collProducts[id - 1].setCate(p.getCategory());
        Database.collProducts[id - 1].setColor(p.getColor());
    }
}
exports.Database = Database;
//# sourceMappingURL=Database.js.map