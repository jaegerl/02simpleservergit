"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Product = void 0;
class Product {
    constructor(name, category, color) {
        this.id = Product.lastId;
        this.name = name;
        this.category = category;
        this.color = color;
        Product.lastId++;
    }
    static fromJson(data) {
        let retProd = new Product(data.name, data.category, data.color);
        retProd.id = data.id;
        return retProd;
    }
    getId() {
        return this.id;
    }
    getName() {
        return this.name;
    }
    getColor() {
        return this.color;
    }
    getCategory() {
        return this.category;
    }
    toString() {
        return "Product: [" + this.id + ", " + this.name + ", " + this.category + ", " + this.color + "]";
    }
    setColor(color) {
        this.color = color;
    }
    setCate(category) {
        this.category = category;
    }
    setName(name) {
        this.name = name;
    }
}
exports.Product = Product;
Product.lastId = 1;
Product.filterId = -1;
//# sourceMappingURL=Product.js.map