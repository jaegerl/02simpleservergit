"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Controller = void 0;
const Database_1 = require("./Database");
const AppError_1 = require("./AppError");
const Product_1 = require("./Product");
const FileOption_1 = require("./FileOption");
class Controller {
    constructor() {
        Controller.option = new FileOption_1.FileOption();
        Controller.option.root = "/AAIF/NFS/Semester5_6_NVS/05Nodes/Overview/02SimpleServerB/wwwPublic";
        Controller.db = new Database_1.Database();
    }
    initProducts(req, res) {
        Controller.db.initProducts();
        console.log("==== init done");
        res.json("==init done==");
    }
    showErrorPage(req, res) {
        console.log("=====error page ..." + req.originalUrl);
        res.status(404).json({
            message: "Can't find " + req.originalUrl + " on this server",
            status: "fail",
        });
    }
    doErrorHandling(err, req, res, next) {
        console.log("=====error handling ..." + req.originalUrl + "..." + JSON.stringify(err));
        err.statusCode = err.statusCode || 500;
        err.status = err.status || "unknown Error";
        res.status(err.statusCode).json(err);
    }
    getAllProducts(req, res) {
        let collFilteredProducts = Controller.db.getAllProducts(req.query.category, req.query.color);
        console.log("===== get all or filtered product");
        res.json(collFilteredProducts);
    }
    getProductById(req, res, next) {
        let filteredProd;
        try {
            filteredProd = Controller.db.getProductById(req.params.id);
        }
        catch (error) {
            let err = new AppError_1.AppError(error.message);
            err.status = "fail";
            err.statusCode = 404;
            next(err);
        }
        console.log("===== get product by id");
        res.json(filteredProd);
    }
    deleteProductById(req, res, next) {
        let filteredProd;
        try {
            let deleledProd = Controller.db.deleteProductByIdDB(req.params.id);
            console.log("Prodact: " + deleledProd.toString() + " was deleted");
            res.json("Prodact: " + deleledProd.toString() + " was deleted");
        }
        catch (error) {
            let err = new AppError_1.AppError(error.message);
            err.status = "fail";
            err.statusCode = 404;
            next(err);
        }
        console.log("===== get product by id");
        res.json(filteredProd);
    }
    updateProductById(req, res, next) {
        try {
            let newProduct = Product_1.Product.fromJson(req.body);
            let id = req.params.id;
            Controller.db.updateProd(newProduct, id);
            console.log("Update done");
            res.json("update done to: " + newProduct.toString());
        }
        catch (error) {
            let err = new AppError_1.AppError("error: " + error.message);
            err.status = "fail";
            err.statusCode = 404;
            next(err);
        }
    }
    addProduct(req, res, next) {
        try {
            let newProduct = Product_1.Product.fromJson(req.body);
            let retProduct = Controller.db.addProduct(newProduct);
            console.log("===== add Product: " + newProduct.toString());
            res.json(retProduct);
        }
        catch (error) {
            let err = new AppError_1.AppError("error: " + error.message);
            err.status = "fail";
            err.statusCode = 400;
            next(err);
        }
    }
    showImagesAndCSS(req, res, next) {
        let url = req.originalUrl;
        console.log("=====show images/CSS..." + url);
        console.log();
        res.sendFile(url, Controller.option);
    }
    showIndexPage(req, res, next) {
        res.sendFile("html/index.html", Controller.option);
        console.log("===== show index.html");
    }
}
exports.Controller = Controller;
//# sourceMappingURL=Controller.js.map